describe("sessionIDDefined", () => {
  it("Should inject the script then verify session ID is defined", () => {
    cy.visit("https://status.cloud.google.com/");
    cy.window().then(() => {
      (async function() {
        var qtm = document.createElement("script");
        qtm.type = "text/javascript";
        qtm.async = 1;
        qtm.src =
          "https://cdn.quantummetric.com/instrumentation/quantum-qa1.js";
        var d = document.getElementsByTagName("script")[0];
        !window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);
      })();
      window["QuantumMetricOnload"] = function() {
        QuantumMetricAPI.addEventListener("start", function() {
          var sessionID = QuantumMetricAPI.getSessionID();
          expect(sessionID).to.be.not.undefined;
        });
      };
    });
  });
});
