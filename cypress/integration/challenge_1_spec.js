describe("Challenge 1", () => {
  it("Should check if the Adobe Analytics Logo is Rendered", () => {
    cy.visit("https://www.quantummetric.com/integrations/adobe-launch");
    cy.contains("Adobe Analytics")
      .prev()
      .should("be.visible");
  });
});
