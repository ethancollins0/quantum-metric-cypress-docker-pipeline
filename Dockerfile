FROM cypress/included:4.0.0

LABEL Ethan Collins <ethancollinsdev@gmail.com>

COPY package.json .
RUN npm i

COPY . .

CMD ["npm", "start"]